"""
Selenium element resolver.
"""
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

class ElementResolver:
    """
    Selenium element resolver.
    """
    driver: WebDriver
    prefix: str

    def __init__(self, driver, prefix='body'):
        self.driver = driver
        self.prefix = prefix

    def find_by_id(self, selector: str):
        """
        Attempt to find the selector by ID.
        """
        if selector[0] == '#':
            return self.driver.find_element(By.ID, selector[1:])
        return self.driver.find_element(By.ID, selector)

    def find_by_selector(self, selector: str) -> WebElement:
        """
        Find element by selector.
        """
        return self.driver.find_element(By.CSS_SELECTOR, self.format(selector))

    def get_by_selector(self, selector: str) -> list[WebElement]:
        """
        Get elements by selector.
        """
        return self.driver.find_elements(By.CSS_SELECTOR, self.format(selector))

    def find_link_element(self, link: str) -> WebElement:
        """
        Find link element.
        """
        return self.driver.find_element(By.CSS_SELECTOR, self.format('a[href*="%s"]' % link))

    def format(self, selector: str) -> str:
        """
        Format the given selector with the current prefix.
        """
        return '%s %s' % (self.prefix, selector)

    def resolve_for_checking(self, selector: str, value) -> WebElement:
        """
        Resolve the element for a given checkbox "field".
        """
        selector = '%s[type="checkbox"]' % selector
        if value is not None:
            selector = '%s[value="%s"]' % (selector, value)
        return self.find_by_selector(selector)

    def resolve_for_selection(self, selector: str) -> WebElement:
        """
        Resolve the element for a given select "field".
        """
        return self.find_by_selector(selector)

    def resolve_for_typing(self, selector: str) -> WebElement:
        """
        Resolve the element for a given input "field".
        """
        return self.find_by_selector(selector)
