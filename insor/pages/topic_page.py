from bs4 import BeautifulSoup
import ssl
from urllib import request
from typing import Generator

from ..page import Page
from insor.pages.word_page import WordPage

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

class TopicPage(Page):
    NEXT_SELECTOR = 'nav .next'
    WORDS_SELECTOR = '.palabras-diccionario article > a'
    url: str

    def __init__(self, url: str):
        self.url = url

    def get_words(self) -> Generator[WordPage, any, None]:
        html = request.urlopen(self.url, context=ctx).read()
        soup = BeautifulSoup(html, 'html.parser')
        for word in soup.select(self.WORDS_SELECTOR):
            yield WordPage(word.get('href'))
        if soup.select_one(self.NEXT_SELECTOR) != None:
            next_page = soup.select_one(self.NEXT_SELECTOR).get('href')
            yield from TopicPage(next_page).get_words()
