from ..page import Page
from visa.pages.schedule_appointment_page import ScheduleAppointmentPage
from web_scraper import settings

class RescheduleAppointmentPage(Page):
    path = '/en-{}/niv/schedule/{}/appointment'.format(settings.SERVICES['visa']['country'], settings.SERVICES['visa']['schedule_id'])

    def commit(self):
        self.browser.press('input[name="commit"]')
        self.browser.wait_for_title('Schedule Appointments | Official U.S. Department of State Visa Appointment Service', 10)
        return ScheduleAppointmentPage(self.browser)

    @property
    def url(self):
        return self.host + self.path
