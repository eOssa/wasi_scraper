import functools
import logging
import schedule
import time

from persistance import logger
from web_scraper.scraper import Scraper
import ciencuadras
import metrocuadrado

schedule_logger = logging.getLogger('schedule')
schedule_logger.setLevel(level=logging.DEBUG)

def catch_exceptions(cancel_on_failure=False):
    def catch_exceptions_decorator(job_func):
        @functools.wraps(job_func)
        def wrapper(*args, **kwargs):
            try:
                return job_func(*args, **kwargs)
            except:
                import traceback
                print(traceback.format_exc())
                if cancel_on_failure:
                    return schedule.CancelJob
        return wrapper
    return catch_exceptions_decorator

def ciencuadras_rent_building():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.BUILDING).scrape()

def ciencuadras_rent_warehouse():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.WAREHOUSE).scrape()

def ciencuadras_rent_land():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.LAND).scrape()

def ciencuadras_rent_consulting_room():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.CONSULTING_ROOM).scrape()

def ciencuadras_rent_farm():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.FARM).scrape()

def ciencuadras_rent_office():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.OFFICE, price_from=100e3, price_to=5e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.OFFICE, price_from=5e6, price_to=5000001).scrape()

def ciencuadras_rent_commercial_office():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.COMMERCIAL_OFFICE, price_from=100e3, price_to=2e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.COMMERCIAL_OFFICE, price_from=2e6, price_to=5e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.COMMERCIAL_OFFICE, price_from=5e6, price_to=5000001).scrape()

def ciencuadras_rent_house():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.HOUSE).scrape()

def ciencuadras_rent_apartment():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.APARTMENT, price_from=100e3, price_to=750e3).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.APARTMENT, price_from=750e3, price_to=1e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.APARTMENT, price_from=1e6, price_to=1.250e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.APARTMENT, price_from=1.250e6, price_to=1.5e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.APARTMENT, price_from=1.5e6, price_to=1.750e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.APARTMENT, price_from=1.750e6, price_to=2e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.APARTMENT, price_from=2e6, price_to=3e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.RENT, property_type=ciencuadras.APARTMENT, price_from=3e6, price_to=5000001).scrape()

def ciencuadras_sale_building():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.BUILDING).scrape()

def ciencuadras_sale_warehouse():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.WAREHOUSE).scrape()

def ciencuadras_sale_land():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.LAND, price_from=10e6, price_to=2500e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.LAND, price_from=2500e6, price_to=5000000001).scrape()

def ciencuadras_sale_consulting_room():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.CONSULTING_ROOM).scrape()

def ciencuadras_sale_farm():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.FARM, price_from=10e6, price_to=2500e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.FARM, price_from=2500e6, price_to=5000000001).scrape()

def ciencuadras_sale_office():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.OFFICE).scrape()

def ciencuadras_sale_commercial_office():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.COMMERCIAL_OFFICE).scrape()

def ciencuadras_sale_house():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.HOUSE, price_from=10e6, price_to=100e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.HOUSE, price_from=100e6, price_to=250e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.HOUSE, price_from=250e6, price_to=375e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.HOUSE, price_from=375e6, price_to=500e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.HOUSE, price_from=500e6, price_to=750e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.HOUSE, price_from=750e6, price_to=1000e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.HOUSE, price_from=1000e6, price_to=2500e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.HOUSE, price_from=2500e6, price_to=5000000001).scrape()

def ciencuadras_sale_apartment():
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=10e6, price_to=100e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=100e6, price_to=150e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=150e6, price_to=175e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=175e6, price_to=200e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=200e6, price_to=212.5e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=212.5e6, price_to=225e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=225e6, price_to=237.5e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=237.5e6, price_to=250e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=250e6, price_to=280e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=280e6, price_to=312.5e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=312.5e6, price_to=375e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=375e6, price_to=437.5e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=437.5e6, price_to=500e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=500e6, price_to=625e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=625e6, price_to=750e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=750e6, price_to=1000e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=1000e6, price_to=2500e6).scrape()
    Scraper(Scraper.CIENCUADRAS, trade_type=ciencuadras.SALE, property_type=ciencuadras.APARTMENT, price_from=2500e6, price_to=5000000001).scrape()

def metrocuadrado_sale_apartment():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=0, price_to=150e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=150e6, price_to=200e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=200e6, price_to=250e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=250e6, price_to=300e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=300e6, price_to=350e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=350e6, price_to=400e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=400e6, price_to=500e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=500e6, price_to=600e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=600e6, price_to=650e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=650e6, price_to=750e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=750e6, price_to=880e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=880e6, price_to=1000e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=1000e6, price_to=1500e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=1500e6, price_to=2000e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENT, price_from=2000e6).scrape()

def metrocuadrado_sale_house():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_HOUSE, price_from=0, price_to=400e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_HOUSE, price_from=400e6, price_to=650e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_HOUSE, price_from=650e6, price_to=1000e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_HOUSE, price_from=1000e6, price_to=2000e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_HOUSE, price_from=2000e6).scrape()

def metrocuadrado_sale_office():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_OFFICE).scrape()

def metrocuadrado_sale_commercial_property():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_COMMERCIAL_PROPERTY).scrape()

def metrocuadrado_sale_warehouse():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_WAREHOUSE).scrape()

def metrocuadrado_sale_estate():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_ESTATE).scrape()

def metrocuadrado_sale_offices_building():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_OFFICES_BUILDING).scrape()

def metrocuadrado_sale_apartments_building():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_APARTMENTS_BUILDING).scrape()

def metrocuadrado_sale_consulting_room():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.SALE, property_type=metrocuadrado.URL_CONSULTING_ROOM).scrape()

def metrocuadrado_rent_apartment():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=0, price_to=1e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=1e6, price_to=1.4e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=1.4e6, price_to=1.6e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=1.6e6, price_to=2e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=2e6, price_to=2.5e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=2.5e6, price_to=3e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=3e6, price_to=4e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=4e6, price_to=6e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=6e6, price_to=9e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENT, price_from=9e6).scrape()

def metrocuadrado_rent_house():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_HOUSE).scrape()

def metrocuadrado_rent_office():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_OFFICE, price_from=0, price_to=6e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_OFFICE, price_from=6e6).scrape()

def metrocuadrado_rent_commercial_property():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_COMMERCIAL_PROPERTY, price_from=0, price_to=9e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_COMMERCIAL_PROPERTY, price_from=9e6).scrape()

def metrocuadrado_rent_warehouse():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_WAREHOUSE, price_from=0, price_to=9e6).scrape()
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_WAREHOUSE, price_from=9e6).scrape()

def metrocuadrado_rent_estate():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_ESTATE).scrape()

def metrocuadrado_rent_offices_building():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_OFFICES_BUILDING).scrape()

def metrocuadrado_rent_apartments_building():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_APARTMENTS_BUILDING).scrape()

def metrocuadrado_rent_consulting_room():
    Scraper(Scraper.METROCUADRADO, trade_type=metrocuadrado.RENT, property_type=metrocuadrado.URL_CONSULTING_ROOM).scrape()

@catch_exceptions()
def panini_schedule():
    Scraper(Scraper.PANINI, collection='manga-ataque-de-los-titanes').scrape()
    Scraper(Scraper.PANINI, collection='one-piece-manga').scrape()
    Scraper(Scraper.PANINI, collection='manga-demon-slayer').scrape()
    Scraper(Scraper.PANINI, collection='manga-dr-stone').scrape()
    Scraper(Scraper.PANINI, collection='manga-my-hero-academia').scrape()
    Scraper(Scraper.PANINI, collection='manga-one-punch-man').scrape()
    Scraper(Scraper.PANINI, collection='naruto-manga').scrape()
    Scraper(Scraper.PANINI, collection='manga-berserk').scrape()
    Scraper(Scraper.PANINI, collection='manga-gantz').scrape()
    Scraper(Scraper.PANINI, collection='manga-hunter-x-hunter').scrape()

@catch_exceptions()
def visa_schedule():
    Scraper(Scraper.VISA).scrape()

schedule.every().hour.do(panini_schedule)
schedule.every(10).minutes.do(visa_schedule)
schedule.run_all()

while True:
    schedule.run_pending()
    time.sleep(1)
