"""
Mixin for waiting for elements to appear.
"""
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from dusk.element_resolver import ElementResolver

class WaitForElementMixin:
    """
    Mixin for waiting for elements to appear.
    """
    driver: WebDriver
    resolver: ElementResolver

    def pause(self, seconds: int):
        """
        Wait for a specified number of seconds.
        """
        self.wait(seconds)

    def wait(self, seconds: int):
        """
        Wait for a specified number of seconds.
        """
        self.driver.implicitly_wait(seconds)

    def wait_for_text(self, text, seconds: int):
        """
        Wait for text to appear.
        """
        WebDriverWait(self.driver, seconds).until(
            EC.text_to_be_present_in_element((By.CSS_SELECTOR, self.resolver.format('')), text)
        )

    def wait_for_element(self, locator: object, seconds: int):
        """
        Wait for an element to appear.
        """
        WebDriverWait(self.driver, seconds).until(
            EC.presence_of_element_located(locator)
        )

    def wait_for_title(self, title: str, seconds: int):
        """
        Wait for the page title contains the specified title.
        """
        WebDriverWait(self.driver, seconds).until(
            EC.title_contains(title)
        )
