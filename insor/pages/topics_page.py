from typing import Generator

from insor.pages.topic_page import TopicPage
from insor.pages.word_page import WordPage
from insor.pages.categories.human_page import HumanPage
from insor.pages.categories.society_page import SocietyPage

class TopicsPage:
    path = '/catdiccionario/temario'

    def get_topics(self) -> Generator[TopicPage, any, None]:
        society_page = SocietyPage()
        for topic_page in society_page.subcategories():
            yield topic_page
        human_page = HumanPage()
        for topic_page in human_page.subcategories():
            yield topic_page
    
    def get_words(self) -> Generator[WordPage, any, None]:
        for topic_page in self.get_topics():
            for word_page in topic_page.get_words():
                yield word_page

    @property
    def url(self):
        return self.host + self.path
