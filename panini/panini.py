from urllib import request, parse
from urllib.error import HTTPError
import json
import math
import re
import ssl
import time

from .page import Page
from .models import PaniniProduct

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

class List(Page):
    path = '/collections/{}'
    perPage = 24

    def get_headers(self) -> dict[str, str]:
        return {
            "Content-type": "application/json",
            "Accept": "application/json",
            "User-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36 OPR/70.0.3728.178",
        }

    def get_data(self, page: int, filters: dict[str, any]) -> list[PaniniProduct]:
        collection = filters['collection']
        headers = self.get_headers()
        req = request.Request((self.get_url()+'/products.json?page={}').format(collection, page), headers=headers, origin_req_host=self.host, method="GET")
        response = request.urlopen(req, context=ctx).read()
        data = json.loads(response.decode())['products']
        products: list[PaniniProduct] = []
        for product in data:
            pattern = '|'.join([
                'ataque de los titanes inside',
                'ataque de los titanes before the fall',
                'ranma 1/2',
            ])
            if re.search(pattern, product['title'], re.IGNORECASE) != None:
                continue
            pattern = r'^([a-zA-Z\s\.]+)\sN[o0]?\.?\s?(\d+).*$'
            match = re.search(pattern, product['title'], re.IGNORECASE)
            if match != None:
                product['volume'] = int(match.group(2))
                serie = match.group(1)
                if serie == 'AOT':
                    serie = 'ATAQUE DE LOS TITANES'
                if serie == 'BOKU NO HERO':
                    serie = 'MY HERO ACADEMIA'
                product['serie'] = serie.upper()
                product['title'] = f'{serie}, Vol. {str(product["volume"]).rjust(3, "0")}'
            product['url'] = self.get_url().format(product['handle']).replace('collections', 'products')
            for i, variant in enumerate(product['variants']):
                product['sku'] = variant['sku']
                product['price'] = float(variant['price'])
                product['available'] = variant['available']
                product['reference'] = product['handle'] + str(variant['id'])
                option1_value = variant['option1'] or ''
                option2_value = variant['option2'] or ''
                option3_value = variant['option3'] or ''
                product['option_value'] = ' '.join([option1_value, option2_value, option3_value]).strip()
                products.append(PaniniProduct(product))
        return products

    def get_meta(self, filters: dict[str, any]) -> dict[str, int]:
        page = 1
        collection = filters['collection']
        while True:
            headers = self.get_headers()
            req = request.Request(self.host+'/collections.json?page={}'.format(page), headers=headers, origin_req_host=self.host, method="GET")
            try:
                response = request.urlopen(req, context=ctx).read()
            except HTTPError:
                print('Blocked! Slepping')
                time.sleep(180)
                print('Retrying')
            cols = json.loads(response.decode())['collections']
            if not cols:
                break
            for col in cols:
                if col['handle'] == collection:
                    return {'total_pages': math.ceil(col['products_count'] / self.perPage) + 1, 'total': col['products_count']}
            page += 1
        return {'total_pages': 0, 'total': 0}

    def get_url(self):
        return self.host + self.path
