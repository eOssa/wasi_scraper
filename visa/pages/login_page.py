from ..page import Page
from visa.pages.reschedule_appointment_page import RescheduleAppointmentPage
from web_scraper import settings

class LoginPage(Page):
    path = '/en-{}/niv/users/sign_in'.format(settings.SERVICES['visa']['country'])

    def login(self) -> RescheduleAppointmentPage:
        self.browser.type('#user_email', settings.SERVICES['visa']['email'])
        self.browser.type('#user_password', settings.SERVICES['visa']['password'])
        self.browser.click('label[for="policy_confirmed"]')
        self.browser.press('input[type="submit"]')
        self.browser.wait_for_title('Groups | Official U.S. Department of State Visa Appointment Service', 10)
        return self.browser.visit(RescheduleAppointmentPage(self.browser))

    @property
    def url(self):
        return self.host + self.path
