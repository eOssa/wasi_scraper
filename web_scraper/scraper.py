import time
import json

from ciencuadras import ciencuadras
from ciencuadras.models import CiencuadrasProperty
from dusk.browser import Browser
from insor.pages.topics_page import TopicsPage
from metrocuadrado import metrocuadrado
from metrocuadrado.models import MetrocuadradoProperty
from panini import panini
from panini.models import PaniniProduct
from persistance import logger, db
from persistance.cache import CacheManager
from report import email
from visa.pages.login_page import LoginPage
from web_driver.web_driver_factory import WebDriverFactory

class Scraper:
    CIENCUADRAS: str = 'ciencuadras'
    METROCUADRADO: str = 'metrocuadrado'
    PANINI: str = 'panini'
    VISA: str = 'visa'
    INSOR: str = 'insor'
    target: str = ''
    filters: dict[str, any] = {}

    def __init__(self, target: str, **kwargs):
        self.target = target
        self.filters = kwargs

    def scrape(self):
        if self.target == self.INSOR:
            return self.scrape_insor()
        if self.target == self.PANINI:
            return self.scrape_panini()
        if self.target in [self.METROCUADRADO, self.CIENCUADRAS]:
            return self.scrape_properties()
        if self.target == self.VISA:
            return self.scrape_visa()

    def scrape_insor(self):
        self.log('started')
        topics_page = TopicsPage()
        for word_page in topics_page.get_words():
            logger.info('Scraping the word [{}]'.format(word_page.sign))
            exists = db.exists_word_by_sign(word_page.sign)
            if not exists:
                db.insert_word({
                    "category": word_page.category,
                    "hand_configuration": word_page.hand_configuration,
                    "sign": word_page.sign,
                    "sign_image": word_page.sign_image,
                    "sign_video": word_page.sign_video,
                    "meaning": word_page.meaning,
                    "meaning_image": word_page.meaning_image,
                    "meaning_video": word_page.meaning_video,
                    "example": word_page.example,
                    "example_image": word_page.example_image,
                    "example_video": word_page.example_video,
                })
            time.sleep(1)
        self.log('finished')

    def scrape_panini(self):
        meta = panini.List().get_meta(self.filters)
        total = meta['total_pages']

        self.log('started')
        new_items = []
        changes = []
        ids = []
        serie = ''
        for page in range(1, total + 1):
            logger.info('{} / {} ({}%)'.format(page, total, round(page / total * 100, 2)))
            for model in panini.List().get_data(page, self.filters):
                serie = model.serie
                product = db.find_property_by_reference(model.reference)
                exists = product is not None
                was_updated = False
                if exists:
                    was_updated = db.product_was_updated_by_reference(model.reference, model.updated_at)
                ids.append(model.id)
                if not exists:
                    db.insert_property(model.__dict__)
                    new_items.append(model)
                if was_updated:
                    db.update_property_by_reference(model.reference, model.__dict__)
                    product_changes = {}
                    if product['price'] != model.price:
                        product_changes['price'] = {'old': product['price'], 'new': model.price}
                    if product['title'] != model.title:
                        product_changes['title'] = {'old': product['title'], 'new': model.title}
                    if product['available'] != model.available:
                        product_changes['available'] = {'old': product['available'], 'new': model.available}
                    if product['option_value'] != model.option_value:
                        product_changes['option_value'] = {'old': product['option_value'], 'new': model.option_value}
                    if product_changes:
                        product_changes['url'] = {'old': product['url'], 'new': model.url}
                        changes.append(product_changes)
            time.sleep(1)
        if serie != '':
            for db_id in db.get_all_products_ids_by_serie(serie):
                if db_id not in ids:
                    db.update_property_by_id(db_id, {'available': False})
        self.new_products_report_by_email(new_items)
        self.updated_products_report_by_email(changes)
        self.log('finished')

    def scrape_properties(self):
        meta = self.get_list_meta()
        total = meta['total_pages']
        cache = CacheManager()
        filters_key = '.' + json.dumps(self.filters) if len(self.filters) else ''
        page_key = self.target + filters_key +  '.page'
        current = cache.get(page_key, 1)

        if (current > total):
            self.log('finished')
            return None

        self.log('started')
        for page in range(current, total + 1):
            cache.put(page_key, page)
            logger.info('{} / {} ({}%)'.format(page, total, round(page / total * 100, 2)))
            data_list = self.get_list_data(page)
            for data in data_list:
                model = data.get_property_model()
                exists = db.exists_property_by_reference(model.reference)
                if not exists and self.validate_and_report(model):
                    db.insert_property(model.__dict__)
            time.sleep(1)
        cache.put(page_key, page + 1)
        self.report_by_email(meta['total'])
        self.log('finished')

    def scrape_visa(self):
        self.log('started')
        browser = Browser(WebDriverFactory.create_web_driver())
        login_page: LoginPage = browser.visit(LoginPage(browser))
        reschedule_appointment_page = login_page.login()
        schedule_appointment_page = reschedule_appointment_page.commit()
        if schedule_appointment_page.available:
            email.send('Appointments availables', schedule_appointment_page.url)
        self.log('finished')
        browser.quit()
    
    def validate_and_report(self, model: CiencuadrasProperty | MetrocuadradoProperty):
        valid = True
        property = model.get_property_model()
        if (property.region_id == 0):
            valid = False
            message = f"""No se encontró la región.

Property: {property.to_json()}

{self.target.capitalize()}: {model.to_json()}"""
            email.send('Región no encontrada', message)

        if (property.city_id == 0):
            valid = False
            message = f"""No se encontró la ciudad.

Property: {property.to_json()}

{self.target.capitalize()}: {model.to_json()}"""
            email.send('Ciudad no encontrada', message)
        return valid

    def get_list_data(self, page):
        return self.get_target_list_obj().get_data(page, self.filters)

    def get_list_meta(self) -> dict[str, int]:
        return self.get_target_list_obj().get_meta(self.filters)
    
    def get_target_list_obj(self):
        if self.target == self.CIENCUADRAS:
            return ciencuadras.List()
        if self.target == self.METROCUADRADO:
            return metrocuadrado.List()
    
    def must_scrape(self) -> bool:
        return self.target in [self.CIENCUADRAS, self.INSOR, self.METROCUADRADO, self.PANINI, self.VISA]
    
    def scrape_label(self) -> str:
        options = []
        if len(self.filters):
            options.append(json.dumps(self.filters))
        type_message: str = '-'.join(options)
        if type_message != '':
            type_message = f' ({type_message})'
        return self.target.capitalize() + type_message
    
    def report_by_email(self, total: int):
        subject = f'Finalizó el raspado a {self.scrape_label()}'
        message = f'Total registros obtenidos {total}'
        email.send(subject, message)

    def new_products_report_by_email(self, data: list[PaniniProduct]):
        if not len(data):
            return
        subject = f'Nuevos productos en {self.scrape_label()}'
        message = ''
        for model in data:
            message += f'- {model.title} ${model.price} -> {model.url}\n'
        email.send(subject, message)

    def updated_products_report_by_email(self, changes: list[dict[str, dict[str, any]]]):
        if not len(changes):
            return
        subject = f'Productos actualizados en {self.scrape_label()}'
        message = ''
        for change in changes:
            change_list = []
            for key, value in change.items():
                if key != 'url':
                    change_list.append(f'[{key}] {value["old"]} -> {value["new"]}')
            message += f'- {", ".join(change_list)} {change["url"]["old"]}\n'
        email.send(subject, message)
    
    def log(self, message: str):
        message = f'Scraping to {self.scrape_label()} {message}'
        logger.info(message)
        print(message)
