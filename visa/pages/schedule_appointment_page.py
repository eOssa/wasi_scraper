from ..page import Page
from web_scraper import settings

class ScheduleAppointmentPage(Page):
    path = '/en-{}/niv/schedule/{}/appointment'.format(settings.SERVICES['visa']['country'], settings.SERVICES['visa']['schedule_id'])

    @property
    def available(self):
        text: str = self.browser.resolver.find_by_selector('').text
        return 'There are no available appointments at the selected location.' not in text and 'error' not in text and 'Maintenance' not in text

    @property
    def url(self):
        return self.host + self.path
