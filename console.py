import json
import sys

from persistance.cache import CacheManager
from persistance.db import SqliteConnector
from web_scraper.scraper import Scraper
from persistance.db import export_properties_to_csv, export_words

action = sys.argv[1]

filters = {}
if (len(sys.argv) > 2):
    filters = json.loads(sys.argv[2])

scraper = Scraper(action, **filters)

if (scraper.must_scrape()):
    scraper.scrape()

if (action == 'cache:flush'):
    cache = CacheManager()
    cache.flush()
    print('Cache deleted')

if (action == 'db:populate'):
    conector = SqliteConnector()
    conector.populate()
    print('DB populated')

if (action == 'export-properties'):
    print('Export properties to CSV file started')
    export_properties_to_csv()
    print('Exported properties to CSV file')

if (action == 'export-words'):
    print('Export words started')
    export_words()
    print('Exported words')
