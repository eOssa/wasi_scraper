import json

class Model():
    def to_json(self):
        return json.dumps(self.__dict__, indent=4, sort_keys=True, default=str)
